# Messenger Analyser `Version 1.30-Web`
Messenger analyser (A Little Lovely Story), generates webpage, mobile, windows, linux, mac applications in multiple languages for a static/dynamic set of FaceBook Messenger data. Generates interactive visualisations and sub-applications to grant entertaining insights to users.

# Features
* Visualise all sorts of data from your chats
* Examples of visualisations: Total messages, Messages by person, Time of contact and more!
* Advanced Autocomplete and Sentiment rating applications
* Group chat support
* Can be hosted as a website to visualise a specfic discussion
* Calling logs

# Usage
## Web (presentation) mode
* Compile with the desired messenger .json files included in the ```/src/public/messenger_data``` data path
* Create a ```manifest.json``` file and list all files that you want included there:
```json
[
  "message_1.json",
  "message_2.json",
  "message_3.json",
  "message_4.json",
  "message_5.json",
  "message_6.json",
  "message_7.json"
]
```
* Run app!

## Electron (native) mode
* Run app, upon loading select the files you would like to visualise.
