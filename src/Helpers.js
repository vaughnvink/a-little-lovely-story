import moment from 'moment'
import _ from 'lodash'
import Sentiment from 'sentiment'

/**
 * Gets sentiment of 'limit' messages in 'order:asc~desc' order from 'corpus'
 * @param  {Object} corpus         Corpus with messages and participants
 * @param  {Number} [limit= 10]    Number of messages to display, -1 for all
 * @param  {String} [order='desc'] asc or desc for order
 * @param  {Number} [minLength= 0] minimum length for a message to be accepted
 * @return {Array}                 Ranked messages by comparative sentiment
 */
export function getRankedSentimentFromCorpus (corpus, limit, order = 'desc', minLength = 0) {
  const exportCorpus = {
    messages: [],
    participants: corpus.participants
  }
  corpus.messages.forEach((message) => {
    if (message.content && message.content.length > minLength) {
      const fixedMessage = fixFacebookMessageISO(message.content)
      const sentiment = getSentimentForString(fixedMessage)
      exportCorpus.messages.push({
        sender_name: message.sender_name,
        content: fixedMessage,
        score: sentiment.score.toFixed(2),
        comparative: sentiment.comparative.toFixed(2),
        scoreN: sentiment.score,
        comparativeN: sentiment.comparative
      })
    }
  })
  exportCorpus.messages = _.orderBy(exportCorpus.messages, 'comparativeN', order).slice(0, limit)
  return exportCorpus
}

export function getSentimentOfCorpus (corpus) {
  return getSentimentForString(corpusToString(corpus))
  // Alternative calculation method, result isn't really different. (non comparative)
  // let total = 0
  // corpus.messages.forEach((message) => {
  //   if (message.content) {
  //     total += getSentimentForString(message.content).comparative
  //   }
  // })
  // return total
}

export function getSentimentForString (strings) {
  const sen = new Sentiment()
  if (Array.isArray(strings)) {
    strings.forEach((string) => {
      string = sen.analyze(string)
    })
    return strings
  } else if (typeof strings === 'string') {
    return sen.analyze(strings)
  }
}

/**
 * Get some regularly used frequencies of a corpus
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Object}        Various frequencies of messages
 */
export function getRegularFrequencies (corpus) {
  const returnable = {}
  returnable.startDate = moment(corpus.messages[corpus.messages.length - 1].timestamp_ms)
  returnable.endDate = moment(corpus.messages[0].timestamp_ms)
  returnable.days = returnable.endDate.diff(returnable.startDate, 'days')
  returnable.perHour = prettyVal(corpus.messages.length / (returnable.days * 24))
  returnable.perDay = prettyVal(corpus.messages.length / (returnable.days))
  returnable.perWeek = prettyVal(corpus.messages.length / (returnable.days / 7))
  returnable.perMonth = prettyVal(corpus.messages.length / (returnable.days / 30))
  returnable.timeTillMessage = moment.duration((3600 / (corpus.messages.length / (returnable.days * 24))), 'seconds')
  return returnable
}

/**
 * Get start and end dates of a corpus
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Object}        startDate: Moment, endDate: Moment
 */
export function getStartAndEndDates (corpus) {
  const startDate = moment(corpus.messages[corpus.messages.length - 1].timestamp_ms)
  const endDate = moment(corpus.messages[0].timestamp_ms)
  return { startDate: startDate, endDate: endDate }
}

export function fixFacebookMessageISO (string) {
  let fixedstring
  try {
    // If the string is UTF-8, this will work and not throw an error.
    fixedstring = decodeURIComponent(escape(string))
  } catch (e) {
    // If it isn't, an error will be thrown, and we can assume that we have an ISO string.
    fixedstring = string
  }
  return fixedstring
}

/**
 * Transform entire corpus to a single string, optional regex transform
 * @param  {Object} corpus           Corpus with messages and participants
 * @param  {String} [regex=/[&#,+(] regex to replace by
 * @return {String}                  All messages glued together
 */
export function corpusToString (corpus, regex = /[&\\#,+()$~%.":*?!<>{}]/g) {
  const corpusJoined = corpus.messages.map(function (elem) {
    return fixFacebookMessageISO(elem.content)
  }).join(' ').toLowerCase().replace(regex, '').replace(/'/g, '’')
  return corpusJoined
}

/**
 * Generates probababilities of the words used after all other words
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Array}        Frequencies
 */
export function wordFollowCount (corpus) {
  const string = corpusToString(corpus, /[&\\#+()$~%"<>{}]/g)
  let frequencies = {}
  const wordList = string.split(' ')
  for (let i = 0; i < wordList.length; i++) {
    const word = wordList[i]
    const nextWord = wordList[i + 1]
    if (word && nextWord) {
      if (!frequencies[word]) {
        // Create word frequency object
        frequencies[word] = {
          word: word,
          count: 0,
          next: {},
          nextTotal: 0
        }
      }
      frequencies[word].count++
      // Create next word count object
      if (!frequencies[word].next[nextWord]) {
        frequencies[word].next[nextWord] = {
          word: nextWord,
          count: 0
        }
      }
      frequencies[word].next[nextWord].count++
      frequencies[word].nextTotal++
    }
  }
  // Transform to arrays
  frequencies = _.orderBy(Object.values(frequencies), 'count', 'desc')
  frequencies.forEach((word, i) => {
    word.next = _.orderBy(Object.values(word.next), 'count', 'desc')
  })

  return frequencies
}

/**
 * Generates a text based on Autocomplete algorithm
 * @param  {Array} frequencies                 Frequencies of each word
 * @param  {Number} [limit=10]                  How many words to generate in text
 * @param  {String} [selectionMethod='maximum'] Algorithm to use for generation
 * @param  {String} start                       Phrase to start generating with
 * @return {String}                             Resulting text
 */
export function generateSentence (frequencies, limit = 10, selectionMethod = 'maximum', start) {
  let startingPhrase = frequencies[0].word
  if (start) {
    startingPhrase = start.split(' ')
    startingPhrase = startingPhrase[startingPhrase.length - 1].toLowerCase()
  }
  const returnSentence = []
  const restrictedWords = []
  // Find first word
  const firstWord = frequencies.find(x => x.word === startingPhrase)
  if (firstWord) {
    returnSentence.push(firstWord)

    for (var i = 0; i < limit; i++) {
      const word = frequencies.find(x => x.word === returnSentence[returnSentence.length - 1].word)
      if (word && word.next.length > 0) {
        switch (selectionMethod) {
          case 'maximum': {
            returnSentence.push(frequencies.find(x => x.word === word.next[0].word))
            break
          }
          case 'psuedorandom': {
            const rnd = Math.floor(Math.random() * (word.nextTotal - 1))
            const limits = []
            let totalSoFar = 0
            for (var o = 0; o < word.next.length; o++) {
              limits.push({
                lower: totalSoFar,
                upper: totalSoFar + word.next[o].count,
                ref: o
              })
              totalSoFar += word.next[o].count
            }
            console.log(rnd)
            console.log(limits)
            limits.forEach((limitPair, i) => {
              if (rnd >= limitPair.lower && rnd <= limitPair.upper) {
                returnSentence.push(frequencies.find(x => x.word === word.next[limitPair.ref].word))
              }
            })
            break
          }
          case 'random': {
            const ref = Math.floor(Math.random() * (word.next.length - 1))
            returnSentence.push(frequencies.find(x => x.word === word.next[ref].word))
            break
          }
          case 'restricted': {
            for (let x = 0; x < word.next.length; x++) {
              if (!restrictedWords.find(z => z === word.next[x].word)) {
                restrictedWords.push(word.next[x].word)
                returnSentence.push(frequencies.find(y => y.word === word.next[x].word))
                break
              }
            }
            break
          }
        }
      }
    }
  }
  return returnSentence
}

/**
 * Order all words in a corpus by their frequency
 * @param  {Object} corpus  Corpus with messages and participants
 * @param  {Number} [n=500] Limit on output unique words from top
 * @return {Array}         Array of words
 */
export function orderCorpusWordsByFrequency (corpus, n = 500) {
  const string = corpusToString(corpus)
  const frequentObj = {}
  const stringArray = string.split(' ')
  for (var i = 0; i < stringArray.length; i++) {
    const word = stringArray[i]
    if (frequentObj[word]) {
      frequentObj[word]++
    } else {
      frequentObj[word] = 1
    }
  }
  const keyVals = Object.entries(frequentObj)
  const sortedNestedArray = keyVals.sort((a, b) => {
    if (a[1] > b[1]) {
      return 1
    } else if (a[1] < b[1]) {
      return -1
    } else {
      return 0
    }
  })
  const flattenedArray = sortedNestedArray.map((arr) => {
    return [arr[0], frequentObj[arr[0]]]
  })
  const resultArray = flattenedArray.reverse()
  // let resultString = flattenedArray.join(' ');
  return resultArray.slice(0, n)
}

/**
 * Builds time series out of corpus
 * @param  {Object} corpus               Corpus with messages and participants
 * @param  {Array} integrationTime       [int (number of units), String (unit)]
 * @param  {Number} [defaultDataPoint=0] Default values for data points
 * @param  {Function} [handler=function    (localDataPoint, message, index)] Handles data for single data point
 * @return {Array}                      Time series for highcharts
 */
export function buildTimeSeries (corpus, integrationTime = [1, 'month'], defaultDataPoint = 0, handler = function (localDataPoint) {
  return localDataPoint + 1
}) {
  const timeSeries = []

  if (corpus.messages.length > 0) {
    const timeLimits = getStartAndEndDates(corpus)

    const localTimeLimits = {
      startDate: timeLimits.endDate.clone().endOf(integrationTime[1]).subtract(integrationTime[0], integrationTime[1]),
      endDate: timeLimits.endDate.clone().startOf(integrationTime[1]).add(2, 'hour')
    }
    let localDataPoint = defaultDataPoint
    corpus.messages.forEach((message, index) => {
      if (moment(message.timestamp_ms).isSameOrAfter(localTimeLimits.startDate)) {
        localDataPoint = handler(localDataPoint, message, index)
      } else {
        timeSeries.push({ x: localTimeLimits.endDate.valueOf(), y: localDataPoint })
        localTimeLimits.startDate.subtract(integrationTime[0], integrationTime[1])
        localTimeLimits.endDate.subtract(integrationTime[0], integrationTime[1])
        localDataPoint = handler(defaultDataPoint, message, index)
      }
    })
  }
  return timeSeries
}

/**
 * Get a filtered corpus where each message fulfills a condition
 * @param  {Object} corpus    Corpus with messages and participants
 * @param  {Function(Message:String, Key:Number)} condition Function with boolean condition return
 * @return {Object}           Filtered corpus
 */
export function getCorpusOnMessageCondition (corpus, condition) {
  const conditionalCorpus = {
    messages: []
  }
  conditionalCorpus.participants = corpus.participants
  corpus.messages.forEach((message, i) => {
    if (condition(message, i)) {
      conditionalCorpus.messages.push(message)
    }
  })
  return conditionalCorpus
}

/**
 * Get the all reactions sorted by type in a semi corpus fashion
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Array}        Array of each reactions
 */
export function getReactions (corpus) {
  const reactions = {}
  corpus.messages.forEach((message, i) => {
    if (message.reactions) {
      message.reactions.forEach((item, i) => {
        if (item.reaction) {
          let fixedReaction = decodeURIComponent(escape(item.reaction))
          // Replace special Covid-19 hearts with normal hearts
          if (fixedReaction === '❤') {
            fixedReaction = '💗'
          }
          if (!reactions[fixedReaction]) {
            reactions[fixedReaction] = {
              name: fixedReaction,
              messages: [],
              participants: corpus.participants
            }
          }
          reactions[fixedReaction].messages.push({
            content: fixedReaction,
            timestamp_ms: message.timestamp_ms,
            sender_name: item.actor
          })
        }
      })
    }
  })
  return Object.values(reactions)
}

/**
 * Get a corpus where all messages contain a specific phrase
 * @param  {Object} corpus Corpus with messages and participants
 * @param  {String} phrase Phrase to filter by
 * @return {Object}        Filtered corpus
 */
export function getCorpusContainingPhrase (corpus, phrase) {
  return getCorpusOnMessageCondition(corpus, (message) => {
    return (message.content && message.content.toLowerCase().includes(phrase.toLowerCase()))
  })
}

/**
 * Returns a filtered corpus by limiting the allowed date range
 * @param  {Object} corpus      Corpus with messages and participants
 * @param  {Array}  [period=[1, 'month']]     [int (number of units), String (unit)]
 * @return {Object}             Filtered corpus
 */
export function getCorpusOfLastPeriod (corpus, period = [1, 'month']) {
  const endDate = moment(corpus.messages[0].timestamp_ms)
  const startDate = moment(endDate).subtract(period[0], period[1])

  const rangedCorpus = {
    messages: []
  }
  rangedCorpus.participants = corpus.participants

  corpus.messages.forEach((message) => {
    if (moment(message.timestamp_ms).isBetween(startDate, endDate)) {
      rangedCorpus.messages.push(message)
    }
  })

  return rangedCorpus
}

/**
 * Returns a float with fixed decimals
 * @param  {Number} x
 * @return {Number}
 */
export function prettyVal (x) {
  return x.toFixed(2)
}

/**
 * Calculate a simple-average
 * @param  {Number} n Part
 * @param  {Number} t Total
 * @return {Number}   Average
 */
export function calcAverage (n, t) {
  return n / t
}

/**
 * Caluclate a percentage
 * @param  {Number} n Part
 * @param  {Number} t Total
 * @return {Number}   Percentage
 */
export function calcPercentage (n, t) {
  return (n / t * 100)
}

/**
 * Converts a unix date to an hour (0-23)
 * @param  {Moment} time UNIX Time
 * @return {Number}      Hour
 */
export function unixToHour (time) {
  return new Date(time).getHours()
}

/**
 * Classify a corpus by 24hr classes
 * @param  {Object} corpus       Corpus with messages and participants
 * @param  {Number} [numsTots=0] Total of messages in full corpus (used for limited corpuses)
 * @return {Array}              Array with 24 hour-values
 */
export function classifyBy24Hr (corpus, numsTots = corpus.messages.length) {
  const timeNums = Array(24).fill(0)
  corpus.messages.forEach((message) => {
    timeNums[unixToHour(message.timestamp_ms)]++
  })
  if (numsTots === 0) {
    numsTots = corpus.messages.length
  }
  timeNums.forEach((time, index) => {
    timeNums[index] = (time / numsTots) * 100
  })
  return timeNums
}

/**
 * Get the names of participants from a corpus
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Array}        Array of people's names
 */
export function getPeoplesNames (corpus) {
  const people = []
  corpus.participants.forEach((person, i) => {
    people.push(person.name)
  })
  return people
}

/**
 * Get number of messages from a corpus
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Number}        Number of messages in corpus
 */
export function getMessagesInCorpus (corpus) {
  return corpus.messages.length
}

/**
 * Get number of words in corpus
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Number}        Number of words in corpus
 */
export function getWordsInCorpus (corpus) {
  let returnable = 0
  corpus.messages.forEach((message, i) => {
    if (message.content !== undefined) {
      returnable += message.content.split(' ').length
    }
  })
  return returnable
}

/**
 * Get number of characters in corpus
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Number}        Number of characters in corpus
 */
export function getCharactersInCorpus (corpus) {
  let returnable = 0
  corpus.messages.forEach((message, i) => {
    if (message.content !== undefined) {
      returnable += message.content.length
    }
  })
  return returnable
}

/**
 * Get proportions of participant messages
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Object}        Messages, words, characters percentages per participant
 */
export function getSenderProportions (corpus) {
  // Seperate corpus on participants
  const splitCorpus = splitOnPerson(corpus)
  const totals = {
    messages: 0,
    words: 0,
    characters: 0
  }
  // For each seperate corpus, get messages, words and characters
  splitCorpus.forEach((corpusPerson, i) => {
    corpusPerson.messagesNums = getMessagesInCorpus(corpusPerson)
    totals.messages += corpusPerson.messagesNums
    corpusPerson.words = getWordsInCorpus(corpusPerson)
    totals.words += corpusPerson.words
    corpusPerson.characters = getCharactersInCorpus(corpusPerson)
    totals.characters += corpusPerson.characters
  })

  // Convert absolute units to percentages
  splitCorpus.forEach((corpusPerson, i) => {
    corpusPerson.messagesPercent = prettyVal(calcPercentage(corpusPerson.messagesNums, totals.messages))
    corpusPerson.wordsPercent = prettyVal(calcPercentage(corpusPerson.words, totals.words))
    corpusPerson.charactersPercent = prettyVal(calcPercentage(corpusPerson.characters, totals.characters))
  })
  return splitCorpus
}

/**
 * Split a corpus on participants
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Array}        Array of corpuses
 */
export function splitOnPerson (corpus) {
  const people = []
  corpus.participants.forEach((person, i) => {
    people.push({ messages: [], participants: [{ name: person.name }] })
  })

  corpus.messages.forEach((message) => {
    corpus.participants.forEach((person, i) => {
      if (message.sender_name === people[i].participants[0].name) {
        people[i].messages.push(message)
      }
    })
  })
  return people
}

/**
 * Classify a corpus' messages split on each participant with 24 hour-classes
 * @param  {Object} corpus Corpus with messages and participants
 * @return {Array}        24hr classifications split on participant
 */
export function classifyBy24HrByPerson (corpus) {
  const messagesSplitOnPerson = splitOnPerson(corpus)
  messagesSplitOnPerson.forEach((personMessages, i) => {
    personMessages = classifyBy24Hr(personMessages, corpus.messages.length)
  })
  return messagesSplitOnPerson
}
