/**
 * Class Visualisation
 * Returns a list of objects with an id (String), type (String), colour (#Hex String)
 */
export const visualisations = [
  {
    id: 'totals',
    type: 'graph',
    colour: '#CC5CB6'
  },
  {
    id: 'who',
    type: 'graph',
    colour: '#8153AD'
  },
  {
    id: 'when',
    type: 'graph',
    colour: '#5A65C2'
  },
  {
    id: 'what',
    type: 'graph',
    colour: '#5090AD'
  },
  {
    id: 'iloveyous',
    type: 'graph',
    colour: '#54CCA9'
  },
  {
    id: 'wordsearch',
    type: 'application',
    colour: '#54CC64'
  },
  {
    id: 'reactions',
    type: 'graph',
    colour: '#D6D45E'
  },
  {
    id: 'sentence',
    type: 'application',
    colour: '#CCB452'
  },
  {
    id: 'sentiment',
    type: 'application',
    colour: '#E6A653'
  },
  {
    id: 'calling',
    type: 'graph',
    colour: '#FF8649'
  }
]
