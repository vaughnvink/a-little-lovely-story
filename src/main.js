import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import i18n from './i18n'
import vuetify from './plugins/vuetify'
import Highcharts from 'highcharts'
import More from 'highcharts/highcharts-more'
import HighchartsVue from 'highcharts-vue'

Vue.config.productionTip = false
More(Highcharts)
Highcharts.setOptions({
  chart: {
    style: {
      fontFamily: 'Roboto'
    },
    zoomType: 'x'
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  }
})
Vue.use(HighchartsVue, {
  highcharts: Highcharts
})

// Set document titles on routed pages
router.afterEach((to, from) => {
  Vue.nextTick(() => {
    document.title = i18n.t('app.title') + ' | ' + i18n.t(to.name)
  })
})

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount('#app')
