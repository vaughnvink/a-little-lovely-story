import Vue from 'vue'
import VueRouter from 'vue-router'
import { visualisations } from '@/Visualisations'

Vue.use(VueRouter)

// Static routes
const routes = [
  {
    path: '/',
    name: 'home.title',
    component: () => import('../views/Home.vue')
  }
]

// Programmatically add routes based on available visual reports
const vis = visualisations
vis.forEach((visualisation) => {
  routes.push({
    name: 'visualisation.' + visualisation.id + '.title',
    path: '/visualisation/' + visualisation.id,
    component: () => import('../views/visualisation/' + visualisation.id + '.vue')
  })
})

// Create new vue router with routes
const router = new VueRouter({
  mode: process.env.IS_ELECTRON ? 'hash' : 'history',
  routes: routes
})

export default router
