import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    landingOpen: false,
    corpus: {
      participants: [],
      messages: []
    }
  },
  mutations: {
    setCorpus (state, newCorpus) {
      state.corpus = newCorpus
    }
  },
  actions: {
  },
  modules: {
  }
})
