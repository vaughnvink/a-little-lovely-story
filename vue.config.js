module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'en-gb',
      fallbackLocale: 'en-gb',
      localeDir: 'locales',
      enableInSFC: false
    },
    electronBuilder: {
      nodeIntegration: true,
      builderOptions: {
        win: {
          icon: 'build/icons/256x256.png',
          target: ['portable']
        }
      }
    }
  },

  transpileDependencies: [
    'vuetify'
  ],

  publicPath: ''
}
